package com.mangroo.temperature;

import com.amazonaws.auth.*;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.mangroo.temperature.business.TemperatureService;
import com.mangroo.temperature.data.DynamoDBConnectionType;
import com.mangroo.temperature.data.repo.TemperatureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;

@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan
@EnableDynamoDBRepositories(basePackages = "com.mangroo.temperature.data.repo")
public class TemperatureConfiguration {

    @Value("${amazon.aws.accesskey}")
    private String amazonAWSAccessKey;

    @Value("${amazon.aws.secretkey}")
    private String amazonAWSSecretKey;

    @Value("${amazon.aws.region}")
    private String region;

    @Value("${amazon.dynamodb.endpoint:}")
    private String amazonDynamoDBEndpoint;

    @Value("${amazon.dynamodb-connection-type}")
    private DynamoDBConnectionType dynamoDBConnectionType;


    Logger logger = LoggerFactory.getLogger(TemperatureConfiguration.class);

    public AWSCredentialsProvider amazonAWSCredentialsProvider() {
        return new AWSStaticCredentialsProvider(amazonAWSCredentials());
    }

    @Bean
    public AWSCredentials amazonAWSCredentials() {
        return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
    }

    @Bean
    public AmazonDynamoDB amazonDynamoDB() {

        logger.info("DynamoDB Connection Type: {}", dynamoDBConnectionType);
        logger.info("DynamoDB Endpoint: {}", amazonDynamoDBEndpoint);
        AmazonDynamoDB amazonDynamoDB;

        switch(dynamoDBConnectionType) {
            case LOCAL: {
                amazonDynamoDB =  AmazonDynamoDBClientBuilder.standard()
                        .withCredentials(amazonAWSCredentialsProvider())
                        .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(amazonDynamoDBEndpoint, "AP_SOUTHEAST_2"))
                        .build();
                break;
            }
            case REMOTE_KEYS: {
                amazonDynamoDB =  AmazonDynamoDBClientBuilder.standard()
                        .withCredentials(amazonAWSCredentialsProvider())
                        .withRegion(Regions.fromName(region))
                        .build();
                break;
            }
            case REMOTE_INSTANCE_PROFILE: {
                amazonDynamoDB =  AmazonDynamoDBClientBuilder.standard()
                        .withCredentials(new InstanceProfileCredentialsProvider(false))
                        .withRegion(Regions.fromName(region))
                        .build();
                break;
            }
            default: {
                amazonDynamoDB =  AmazonDynamoDBClientBuilder.defaultClient();
                break;
            }
        }

        return amazonDynamoDB;
    }

    @Bean
    public TemperatureService temperatureService(
            TemperatureRepository temperatureRepository) {
        return new TemperatureService(temperatureRepository);
    }

}
