package com.mangroo.application.web;

import com.mangroo.application.ApplicationConfiguration;
import com.mangroo.temperature.business.TemperatureService;
import com.mangroo.temperature.data.Temperature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/application")
public class ApplicationController {

    @Autowired
    private ApplicationConfiguration config;

    @Autowired
    private TemperatureService temperatureService;

    private Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    @RequestMapping(method={RequestMethod.GET},value={"/version"})
    public String getVersion() {
        logger.info("Application: {}", config.getName());
        return "1.18 " + config.getName();
    }

    @RequestMapping(method={RequestMethod.GET},value={"/all"})
    public Iterable<Temperature> getTempeatures() {
        logger.info("Application: {}", config.getName());
        return temperatureService.findAll();
    }

    @RequestMapping(method={RequestMethod.POST})
    public Temperature putTempeatures(@RequestBody Temperature temperature) {
        logger.info("Application: {}", config.getName());
        return temperatureService.save(temperature);
    }



}